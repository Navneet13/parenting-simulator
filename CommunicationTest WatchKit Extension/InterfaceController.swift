//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {

    
    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
   
    @IBOutlet var hibernateButton: WKInterfaceButton!
    
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    
    //MARK: Other Variables
    var pikachuImageView = UIImage(named: "pikachu")!
    var caterpieImageVIew = UIImage(named: "caterpie")!
    var defaultImageView = UIImage(named: "pokeball")!
     var gameTimer: Timer?
    var messageBody: String?
    var pokemonState: String?
    var isGameActive = true
    var isHungry = false
    var healthLevel = 100
    var hungerLevel = 0
    var isFeeding = false
    var isHibernating = false
    var isAwake = true
    
    
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
        
        messageBody = message["pokemon"] as! String
        messageLabel.setText(messageBody)
        
        if(messageBody == "pikachu"){
            pokemonImageView.setImage(pikachuImageView)
        }
        else if(messageBody == "caterpie"){
            pokemonImageView.setImage(caterpieImageVIew)
        }
    }
    
    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    
        
        // 1. Check if the watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        if let pokemonName: String = context as? String {
            self.nameLabel.setText(pokemonName)
            self.messageLabel.setText("Your baby is \(pokemonName)")
        }
        else {
            self.nameLabel.setText("")
            self.messageLabel.setText("")
        }
        
        gameTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector( gameUpdation), userInfo: nil, repeats: true)
        
        gameTimer?.invalidate()
        
      
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
//        gameTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(
//            (), userInfo: nil, repeats: true)
//
//        gameTimer?.invalidate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending msg to watch")
            WCSession.default.sendMessage(
                ["name" : "Pritesh"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
    }
    
    
    // MARK: Functions for Pokemon Parenting
    @IBAction func nameButtonPressed() {
        print("name button pressed")
    }

    @IBAction func wakeButtonPressed() {
        print("Wake button pressed")
             isAwake = true
             isHibernating = false
              
             isGameActive = true
             hibernateButton.setEnabled(true)
             
    }
    
    @IBAction func feedButtonPressed() {
        print("Feed button pressed")
        isHungry = true
        isFeeding = true
        pokemonState = "Hungry"
    }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
        isHibernating = true
        isAwake = false
        pokemonState = "Hibernating"
        isGameActive = false
        
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending games state to watch")
            WCSession.default.sendMessage(
                ["state" : "Hibernating"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
        
        hibernateButton.setEnabled(false)
        messageLabel.setText("Game in hibernate mode, Press the Wake Up Button to restart")
        
        
    }
    
    @objc
    func gameUpdation(){
        //Updating Hunger Level
        if(isFeeding == true){
             hungerLevel = hungerLevel - 12
        }
        else{
             hungerLevel = hungerLevel + 10
        }
        
        //Updating Health Level
        if(hungerLevel == 80){
            healthLevel = healthLevel - 5
        }
        
        //Check for valid Health Levels
        if(healthLevel < 0 || healthLevel > 100){
            healthLevel = 100
        }
        
        //If Health Level reaches zero, player dies
        if(healthLevel == 0){
            hungerLevel = 0
            healthLevel = 100
            messageLabel.setText("Game Over!!")
            nameLabel.setText("No Player Selected!")
        }
        
        //Updating the label values
        outputLabel.setText("HP: \(healthLevel) Hunger: \(hungerLevel)")
    }
    
    
    
    
     
}
